## Seacher (master)

 Поиск осуществляется командой data:get.

    $ php artisan data:get {key_file?} {data_file?}

 Где,
 
   key_file 	- файл содержащий в себе искомое значение ключа, обладает дефолтным значением "key_file",
   
   data_file 	- файл содержащий данные, типа 'key\tvalue\x0A', обладает дефолтным значением "data_file".
   
   
## New Task (newtask)

 #Task:cities {array?}
 
    - input ["London", "Paris", "Wild roofs"]  (default: ["Москва", "Санкт-Петербург", "Воронеж"])
    
    - output "London, Paris, Wild roofs."
 
 
 #Task:computers {count}
 
    - input 34 | 12 | 31
    
    - output "Компьютера" | "Компьютеров" | "Компьютер"
    
 
 #Task:prime {number}
 
    - input 1 | 12 
    
    - output "Число составное" | "Число простое" 
 
 #Task:rounder {number}
 
    - input 125 | 63,5
    
    - output 130 | 60 
 
 
 #Task:three {array?}
 
     - input 1_2_3_4_5_6_7_8_9  (default: 3_4_5_6_7_8_9_10_11)
    
    - output (Количество комбинаций соответствующих условию задачи) 72
