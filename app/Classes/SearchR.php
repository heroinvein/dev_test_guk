<?php
/**
 * Created by PhpStorm.
 * User: Vladimir
 * Date: 22.02.2018
 * Time: 22:47
 */

namespace App\Classes;

use Symfony\Component\Console\Output\ConsoleOutput;


class SearchR
{

    //имя файла
    protected $data_file;
    //путь к файлу
    protected $path;
    //значение искомого ключа
    protected $key;
    //текущая позиция указателя
    protected $count;
    //предыдущая позиция указателя
    protected $old_count;
    //значение шага смещения
    protected $step;
    //вывод в консоль
    protected $output;

    /**
     * Входящие данные
     *  - Имя файла содержащего значение искомого ключа
     *  - Имя файла содержащего данные
     *
     * @param $key_file
     * @param $data_file
     */
    public function start($key_file, $data_file) {

        //Начало работы
        $start_time = microtime();

        //Получение значения искомого ключа
        $file_key = fopen($this->path.$key_file, 'r');
        $key = fgets($file_key);
        fclose($file_key);

        $this->data_file    = $data_file;
        $this->path         = '';
        $this->key          = $key;
        $this->output       = new ConsoleOutput();

        $result = $this->getValue();

        //Окончание работы
        $end_time = microtime();

        //Время работы
        $time = $end_time - $start_time;

        $this->output->writeln("\n    Key:    ".$this->key);
        $this->output->writeln('    Result: '.$result."\n");
        $this->output->writeln('    Time works: '.$time." sec\n");

    }


    /**
     * @return string
     */
    protected function getValue() {

        $file = fopen($this->data_file, "r");

        //перемещение файлового указателя в конец файла
        fseek($file, 0,SEEK_END);

        //получение позиции файлового указателя
        $tell = ftell($file);

        //задание начальных параметров
        $this->old_count = round($tell);
        $this->count = round($tell / 2);
        $this->step = round($this->old_count - $this->count);

        $result = 'undef';

        $lines = ['basiclines', 'secondline', 'firstline'];

        foreach ($lines as $func) {

            if ($result == 'undef') {

                $result = $this->$func($file);

            }
            else {

                break;

            }

         }


        fclose($file);

        return $result;

    }

    /**
     * @param $line
     * @return string
     */
    protected function searcher($line)
    {

        //обновляем значение прошлого шага
        $this->old_count = $this->count;

        $value = 'undef';

        //получаем ключ из текущей линии
        $reg_k = "/(.+)\t/";
        $res_k = preg_match_all($reg_k, $line, $mathes);

        //полачаем результат сравнения искомого с текущим ключем
        $res_k ? $bias = $this->strbias($this->key, $mathes[1][0]) : $bias = -1;

        switch ($bias) {

            case  1:

                //если искомое значение больше текущего
                $this->count = round($this->count + $this->step / 2);

                break;

            case  0:

                $reg_v = "/\t(.+)$/";

                preg_match_all($reg_v, $line, $mathes);

                $value = $mathes[1][0];

                break;

            case  -1:

                //если искомое значение меньше текущего
                $this->count = round($this->count - $this->step / 2);

                break;

        }

        //обновляем значение шага
        $this->step = abs($this->old_count - $this->count);

        return $value;

    }

    /**
     * @param $stra
     * @param $strb
     * @return int
     */
    protected function strbias($stra, $strb) {

        //сравниваем длинны строк
        $res = strlen($stra) - strlen($strb);

        if($res > 0 ){

            //если первая строка длиннее
            return 1;

        }
        elseif ($res === 0) {

            //если строки одинаковой длинны, уточняем сравнение
            return strcmp($stra, $strb);

        }
        else {

            //если первая строка короче
            return -1;

        }

    }

    /**
     * Получение первой строки
     *
     * @param $file
     * @return bool|string
     */
    protected  function firstline($file) {

        fseek($file, 0, SEEK_SET);

        $line = fgets($file);

        $result = $this->searcher($line);

        return $result;

    }

    /**
     * Получение второй строки
     *
     * @param $file
     * @return bool|string
     */
    protected function secondline($file) {

        fseek($file, 0, SEEK_SET);

        fgets($file);
        $line = fgets($file);

        $result = $this->searcher($line);

        return $result;

    }

    /**
     * @param $file
     * @return string
     */
    protected function basiclines($file) {

        $result = 'undef';

        while(true) {

            //если шаг больше 1 продолжаем поиск
            if( $this->step > 1 ) {

                //смещение файлового указателя на новую позицию
                fseek($file, $this->count, SEEK_SET);

                //получение следующей цельной строки
                fgets($file);
                $line = fgets($file);

                $result = $this->searcher($line);

                //выход из цикла при достижении результата
                if($result != 'undef') {

                    break;

                }

            }
            else {

                break;

            }

        }

        return $result;

    }

}