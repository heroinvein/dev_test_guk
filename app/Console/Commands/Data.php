<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Classes\SearchR;

class Data extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'data:get {key_file?} {data_file?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $search = new SearchR();

        empty($this->argument('key_file')) ? $key_file = 'key_file' : $key_file = $this->argument('key_file');
        empty($this->argument('data_file')) ? $data_file = 'data_file' : $data_file = $this->argument('data_file');

        $search->start($key_file, $data_file);

    }
}
